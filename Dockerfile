FROM alpine:edge AS build

MAINTAINER Carson Page <carson.page@byterealm.com>

ENV VERSION v0.5.0

RUN apk update && apk add rust cargo cmake build-base coreutils openssl-dev libsass-dev

RUN mkdir -p /usr/src/zola

RUN wget https://github.com/getzola/zola/archive/${VERSION}.tar.gz -O zola.tar.gz && \
    tar -xzf zola.tar.gz --strip-components=1 -C /usr/src/zola && \
    rm zola.tar.gz && cd /usr/src/zola

WORKDIR /usr/src/zola

RUN cargo build --release

FROM alpine:edge

EXPOSE 80

RUN apk update && apk add ca-certificates

COPY --from=build /usr/src/zola/target/release/zola /usr/bin/zola
RUN chmod +x /usr/bin/zola && mkdir /workdir
WORKDIR /workdir
CMD ["zola", "serve", "--port=80", "--interface=0.0.0.0"]
